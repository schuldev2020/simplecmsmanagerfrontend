import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CmsComponent, CmsField } from '../models';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-admincockpit',
  templateUrl: './admincockpit.component.html',
  styleUrls: ['./admincockpit.component.scss']
})
export class AdmincockpitComponent implements OnInit {
  dialogIsOpen: boolean = false;
  components: CmsComponent[] = [];
  newComponentName: string = '';
  newComponentDescription: string = '';

  constructor(private http: HttpClient, private route: ActivatedRoute, private title: Title, private router: Router) { }

  ngOnInit() {
    this.title.setTitle('CMS Cockpit Administration');
    this.http.get<CmsComponent[]>('http://' + window.location.hostname + ':8080/components').toPromise().then(x => {
      this.components = x;
    });

    document.addEventListener('keyup', this.escListener.bind(this));
  }

  removeComponent(id: number) {
    let component = this.components.filter(x => x.id === id)[0];
    if (window.confirm('Die Komponente ' + component.name + ' löschen?')) {
      let url = 'http://' + window.location.hostname + ':8080/component/' + id;
      this.http.delete(url).toPromise().then(x => {
        this.components.splice(this.components.indexOf(component), 1);
      });
    }
  }

  clickDialog(event: Event) {
    let element = event.target as HTMLElement;
    if (element.classList.value === 'dialog-underlay')
      this.dialogIsOpen = false;
  }

  escListener(event: KeyboardEvent) {
    if (this.dialogIsOpen && event.key.toLowerCase() === 'escape')
      this.dialogIsOpen = false;
  }

  newComponentValuesAreValid(): boolean {
    if (this.components.filter(x => x.name.toLowerCase() === this.newComponentName.trim().toLowerCase()).length > 0)
      return false;
    if (this.newComponentName.trim() === '')
      return false;
    return true;
  }

  addComponent(): void {
    let newComponent = new CmsComponent(this.newComponentName, this.newComponentDescription, []);
    this.dialogIsOpen = false;
    this.newComponentName = '';
    this.newComponentDescription = '';
    this.http.put<CmsComponent>('http://' + window.location.hostname + ':8080/components', newComponent).toPromise().then(x => {
      this.components.push(x);
    });
  }

  componentHasFields(component: CmsComponent): boolean {
    return component.cmsFieldList.length > 0;
  }
}
