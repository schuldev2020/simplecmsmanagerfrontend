import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmincockpitComponent } from './admincockpit.component';

describe('AdmincockpitComponent', () => {
  let component: AdmincockpitComponent;
  let fixture: ComponentFixture<AdmincockpitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmincockpitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmincockpitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
