export class CmsField {
    id: number;
    name: string;
    description: string;
    text: string;
  
    constructor(name: string, description: string) {
        this.id = -1;
        this.name = name;
        this.description = description;
        this.text = '';
    }
}

export class CmsComponent {
    id: number;
    name: string;
    description: string;
    lastModified: string;
    cmsFieldList: CmsField[];
    cmsItemList: CmsComponent[];

    constructor(componentName: string, componentDescription: string, registeredFields: CmsField[]) {
        this.name = componentName;
        this.description = componentDescription;
        this.cmsFieldList = registeredFields;
    }
}