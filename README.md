# SimpleCmsManagerFrontend

Angular Frontend Application.

**Getting Started**

# Requirements

* Internet connection
* A common version of node.js (>=v12.14.0) with a common version of npm (>=6.13.4)
* Node 'Express' package
* Angular CLI (>=8.3.26)

**Install node.js and npm**

https://nodejs.org/en/

**Install Angular CLI**

* `npm install -g @angular/cli`

**Check node.js and npm**

* `node -v`
* `npm -v`
* `ng version`

# Install dependencies

* `npm install`

# Start application in development mode

* `ng serve`
* The application will be available on port 4200

# Deploy application in production mode

* `npm run deploy`

# Run application

* `npm run start`

# Usage

* Start the application
* Visit '`http://localhost/`' ('`http://localhost:4200/`' in development mode)
