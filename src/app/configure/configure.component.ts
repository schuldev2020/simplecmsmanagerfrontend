import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CmsComponent, CmsField } from '../models';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  styleUrls: ['./configure.component.scss']
})
export class ConfigureComponent implements OnInit {
  id: number = -1;
  component: CmsComponent;
  components: CmsComponent[] = [];
  originalJson: string = '';


  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private title: Title) {
    this.component = new CmsComponent('', '', []);
  }

  ngOnInit(): void {
    this.title.setTitle('CMS Cockpit Administration');
    this.id = parseInt(this.route.snapshot.queryParamMap.get('id'));
    this.http.get<CmsComponent[]>('http://' + window.location.hostname + ':8080/components').toPromise().then(x => {
      this.components = x;

      let according = x.filter(x => x.id === this.id);

      if (according.length === 0)
        this.router.navigate(['/admin']);

      this.component = according[0];
      this.originalJson = JSON.stringify(this.component);
    });
  }

  addField(): void {
    this.component.cmsFieldList.push(new CmsField('', ''));
  }

  removeField(field: CmsField): void {
    if (field.text.trim() !== '')
      if (!window.confirm('Das Feld ist nicht leer. Trotzdem löschen?'))
        return;

    this.component.cmsFieldList.splice(this.component.cmsFieldList.indexOf(field), 1);
  }

  valuesHaveChanged(): boolean {
    return JSON.stringify(this.component) !== this.originalJson;
  }

  valuesAreValid(): boolean {
    if (this.component.name.trim() === '')
      return false;
    if (this.components.filter(x => x.id !== this.component.id).filter(x => x.name.toLowerCase() === this.component.name.trim().toLowerCase()).length > 0)
      return false;

    let fieldValues: string[] = [];

    for (let i = 0; i < this.component.cmsFieldList.length; i++) {
      if (fieldValues.indexOf(this.component.cmsFieldList[i].name) !== -1)
        return false;
      if (this.component.cmsFieldList[i].name.trim() === '')
        return false;
      fieldValues.push(this.component.cmsFieldList[i].name);
    }

    return true;
  }

  saveChanges(): void {
    this.http.patch('http://' + window.location.hostname + ':8080/component/' + this.component.id, this.component).toPromise().then(x => {
      this.originalJson = JSON.stringify(this.component);
    });
  }

  componentHasFields(): boolean {
    return JSON.parse(this.originalJson).cmsFieldList.length > 0;
  }
}
