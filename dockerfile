FROM node
COPY . /usr/local/app
WORKDIR /usr/local/app
RUN npm run deploy
CMD npm run start
