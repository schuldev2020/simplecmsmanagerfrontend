
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartComponent } from './start/start.component';

import { AdmincockpitComponent } from './admincockpit/admincockpit.component';
import { EditComponent } from './edit/edit.component';
import { ConfigureComponent } from './configure/configure.component';

const routes: Routes = [
  { path: '', component: StartComponent },
  { path: 'view', component: StartComponent },
  { path: 'admin', component: AdmincockpitComponent },
  { path: 'configure', component: ConfigureComponent },
  { path: 'edit', component: EditComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
