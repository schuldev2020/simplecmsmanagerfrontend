import { Component, OnInit } from '@angular/core';
import { CmsField, CmsComponent } from '../models';
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  id: number;
  components: CmsComponent[] = [];
  component: CmsComponent;
  emptyComponent: CmsComponent;
  componentLoaded: boolean = false;

  constructor(private http: HttpClient, private route: ActivatedRoute, private title: Title) {
    this.emptyComponent = new CmsComponent('Willkommen im Simple CMS!', '', []);
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.queryParamMap.get('id'));
    this.component = this.emptyComponent;
    this.title.setTitle('Simple CMS');

    this.http.get<CmsComponent[]>('http://' + window.location.hostname + ':8080/components').toPromise().then(x => {
      this.components = x;
      let according = x.filter(y => y.id === this.id);

      if (according.length !== 0){
        this.component = according[0];
        this.title.setTitle(this.component.name + ' - Simple CMS');
        this.componentLoaded = true;
      }
    });
  }

  changeSelection(id: number): void {
    try {
      this.id = id;
      this.component = this.components.filter(x => x.id === id)[0];
      this.title.setTitle(this.component.name + ' - Simple CMS');
      this.componentLoaded = true;
    }
    catch (ex) {
      this.component = this.emptyComponent;
    }
  }

  formatDate(dateString: string) {
    let date = new Date(dateString);
    return date.toLocaleDateString();
  }

  hasDescription(field: CmsField): boolean {
    return field.description.trim() !== '';
  }

}
