import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CmsComponent, CmsField } from '../models';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  components: CmsComponent[] = [];
  originalFields: CmsField[] = [];
  component: CmsComponent;
  id: number = -1;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private title: Title) {
    this.component = new CmsComponent('', '', []);
  }

  ngOnInit() {
    this.title.setTitle('CMS Cockpit Administration');
    this.id = parseInt(this.route.snapshot.queryParamMap.get('id'));

    this.http.get<CmsComponent[]>('http://' + window.location.hostname + ':8080/components').toPromise().then(x => {
      this.components = x;
      let according = x.filter(x => x.id == this.id);

      if (according.length === 0)
        this.router.navigate(['/admin']);

      this.component = according[0];
      this.originalFields = JSON.parse(JSON.stringify(according[0].cmsFieldList))
    });
  }

  valuesHaveChanged(): boolean {
    return JSON.stringify(this.component.cmsFieldList) != JSON.stringify(this.originalFields);
  }

  saveChanges() {
    this.http.patch('http://' + window.location.hostname + ':8080/component/' + this.component.id, this.component).toPromise().then(x => {
      this.originalFields = JSON.parse(JSON.stringify(this.component.cmsFieldList));
    });
  }

}
