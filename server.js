const port = 4200;
var express = require('express');
var app = express();

app.use(express.static('dist/CmsCockpit'));

app.get('/', (req, res, next) => {
    res.redirect('/');
});

app.listen(port, '0.0.0.0');
console.log('CmsCockpit Server is running on port ' + port);
